# CHANGELOG

## 1.0.2 - 2024-01-30

### Fixed

- Utiliser `{id_?}` comme critère dans le modèles `lesauteurs`
