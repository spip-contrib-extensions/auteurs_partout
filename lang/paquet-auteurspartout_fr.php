<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'auteurspartout_description' => 'Associer des auteurs à tous les objets',
	'auteurspartout_titre' => 'Auteurs partout',
	'auteurspartout_slogan' => 'Des auteurs pour tous les objets',
);

?>
